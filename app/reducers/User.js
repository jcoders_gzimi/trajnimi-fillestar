let defaultState = {
}

/**
 * the user state reducer
 * @param {object} state - the current state
 * @param {action} action - the action to reduce
 * @return {object} the new state
 */
function user (state = {}, action) {
}
/**
 * the session state reducer
 * @param {object} state - the current state
 * @param {action} action - the action to reduce
 * @return {object} the new state
 */
function session (state = defaultState, action) {
  switch (action.type) {
    default:
      return state
  }
}

export default session
