import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField/TextField";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    row: {
      width: '100%',
      marginTop: 8
    },
    rreshti: {
      marginTop: 4
    },
    avatar: {
      margin: 10,
    },
    orangeAvatar: {
      margin: 10,
      color: '#fff',
      backgroundColor: 'orange'
    },
  }
}

/**
 * Aktiviteti 1, ky eshte nje komment per klasen ne vazhdim
 */
export class Aktiviteti1 extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      a: 0,
      b: 0
    }
  }


  onChange = (field, event) => {
    const value = event.target.value
    if (!isNaN(value)) {
      this.setState({
        [field]: parseInt(value)
      })
    }
  }

  renderojEmrat (cilet) {

  }

  shuma (a, b) {
    return a + b
  }

  shumzimi (a, b) {
    return a * b
  }

  renderoUserin = (user, index) => {
    if (user.emri === 'Agon') {
      return (
        <div key={index}>
          Ma interesant o ky user: {user.emri}
        </div>
      )
    }
    return (
      <div key={index}>
        {user.emri}
      </div>
    )
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    let { a, b } = this.state

    const userat = [
      {
        emri: "Agon",
        mbiemri: "Lohaj",
        gjinia: 'm',
        pare: 50
      },
      {
        emri: "Fiona",
        mbiemri: "Shehu",
        gjinia: 'f',
        pare: 100
      },
      {
        emri: "Gzim",
        mbiemri: "Mehmeti",
        gjinia: 'm',
        pare: 90
      }
    ]
    const meshkujt = userat.filter(next => next.gjinia === 'm')
    const femrat = userat.filter(next => next.gjinia === 'f')

    if (!a) {
      b += 2
    } else if(!b) {
      a += 2
    } else {
      a = 10
    }



    let list = userat.map(this.renderoUserin)

    let mashkullore = userat.filter(user => user.gjinia === 'm')

    let shuma = userat.reduce((total, user, index) => {
      return total + user.pare
    }, 0)


    let duplikate = userat.reduce((lista, user, index) => {
      lista.push(user)
      lista.push(user)
      return lista
    }, [])

    let object = {
      emri:"agon",
      mbiemri: "Lohaj",
      gjinia: 'm'
    }


    let objekti2 = object.emri



    const shumzimi = 0
    return (
      <Faqe>
        <h4>
          Miresevini te aktiviteti 1
        </h4>
        <div>
          Ketu do te mesoni si te shumezoni dy variabla per me fitu rezultatin
        </div>
        <TextField
          label={"Variabla A"}
          value={a}
          type={'numeric'}
          onChange={this.onChange.bind(this, 'a')}
          className={classes.row}>
        </TextField>
        <TextField
          label={"Variabla B"}
          value={b}
          type={'numeric'}
          onChange={this.onChange.bind(this, 'b')}
          className={classes.row}>
        </TextField>
        <h5>
          Rezultati a + b
        </h5>
        <h3>
          Rreshti 2
        </h3>
        <div>
          {shuma}
        </div>
        <h5>
          Rezultati a * b
        </h5>
        <div>
          {shumzimi}
        </div>
        <div className={classes.rreshti}>
          Userat ne databaze
        </div>
        <div className={classes.rreshti}>
          {userat.map(next => next.emri).join(',')}
        </div>
        <div className={classes.rreshti}>
          Userat te gjinise mashkuller
        </div>
        <div className={classes.rreshti}>
          {meshkujt.map(next => next.emri).join(',')}
        </div>
        <div className={classes.rreshti}>
          Userat te gjinise femrore
        </div>
        <div className={classes.rreshti}>
          {femrat.map(next => next.emri).join(',')}
        </div>
        <div className={classes.rreshti}>
          Emri + Mbieri
        </div>
        <div className={classes.rreshti}>
          {userat.map(next => next.emri + ' ' + next.mbiemri).join(',')}
        </div>


        <Avatar className={classes.avatar}>H</Avatar>
        <Avatar className={classes.orangeAvatar}>N</Avatar>

        {list}
        {duplikate.map(next => next.emri).join(' edhe ')}
        <br/>
        {mashkullore.map(next => next.emri).join(' mashkulli tjeter ')}
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Aktiviteti1))
