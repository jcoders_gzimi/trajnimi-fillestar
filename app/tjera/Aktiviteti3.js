import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'

import Faqe from 'common/Faqe'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

import Chart from 'common/Graf'

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
  }
}

/**
 * Aktiviteti 3, ky eshte nje komment per klasen ne vazhdim
 */
export class Aktiviteti3 extends React.Component {


  render() {
    let {classes, className, theme: { palette }} = this.props

    const form = {
      marginTop: 4,
      width: '100%'
    }

    var a = 4
    let b = 9
    const users = []


    let rezultati = 'Agoni'
    var labelRight = {
      normal: {
        position: 'right'
      }
    };

    const x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    const y = x.map(next => {
      return [next, Math.pow(next, 2) + 3]
    })
    return (
      <Faqe>
        <Paper style={{
          width: 480,
          margin: '0 auto',
          backgroundColor: 'white',
          padding: 16}}
        >

          <Chart style={{width: '100%', height: 400}} options={{
            title: {
              text: "x^2 + 3"
            },
            xAxis: {
              type: 'value'
            },
            yAxis: {
              type: 'value'
            },
            series: [{
              data: y,
              type: 'line'
            }]
          }}/>
        </Paper>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Aktiviteti3))
