import React, { Children } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import RreshtiUser from 'tjera/RreshtiUser'
import UserCard from 'tjera/UserCard'

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    row: {
      width: '100%',
      marginTop: 8
    },
    rreshti: {
      marginTop: 4
    },
    avatar: {
      margin: 10,
    },
    orangeAvatar: {
      margin: 10,
      color: '#fff',
      backgroundColor: 'orange'
    },
  }
}

/**
 * Aktiviteti 1, ky eshte nje komment per klasen ne vazhdim
 */
export class Compozim extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
    }
  }

  renderoCard (next, index) {
    return <UserCard key={index} user={next}/>
  }

  renderoRresht (next, index) {
    return <RreshtiUser key={index} user={next}/>
  }

  render() {
    let {classes, className, theme: { palette }, users = []} = this.props
    return (
      <div>
        {users.map((next, index) => {
          if (index === 0) {
            return this.renderoCard(next, index)
          }
          return this.renderoRresht(next, index)
        })}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Compozim))
