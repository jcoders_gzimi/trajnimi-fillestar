import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'

import Faqe from 'common/Faqe'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
  }
}

/**
 * Aktiviteti 2, ky eshte nje komment per klasen ne vazhdim
 */
export class Aktiviteti2 extends React.Component {


  render() {
    let {classes, className, theme: { palette }} = this.props

    const form = {
      marginTop: 4,
      width: '100%'
    }

    var a = 4
    let b = 9
    const users = []


    let rezultati = 'Agoni'



    return (
      <Faqe>
        <Paper style={{
          width: 480,
          margin: '0 auto',
          backgroundColor: 'white',
          padding: 16
        }}>
          <TextField color={'primary'} variant={'raised'} label={"Emri"} placeholder={"Shkruaje Emrin"} style={form}>
          </TextField>
          <TextField color={'primary'} variant={'raised'} label={"Mbiemri"} placeholder={"Shkruaje Emrin"} style={form}>
          </TextField>
          <TextField color={'primary'} variant={'raised'} label={"Mosha"} placeholder={"Shkruaje Emrin"} style={form}>
          </TextField>
          <TextField color={'primary'} variant={'raised'} label={"Gjinia"} placeholder={"Shkruaje Emrin"} style={form}>
          </TextField>
          <TextField color={'primary'} variant={'raised'} label={"UserName"} placeholder={"Shkruaje Emrin"} style={form}>
          </TextField>
          <Button color={'primary'} variant={'raised'} style={form}>
            {rezultati}
          </Button>
        </Paper>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Aktiviteti2))
