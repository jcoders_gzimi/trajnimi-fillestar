import React, { Children } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    row: {
      width: '100%',
      marginTop: 8
    },
    rreshti: {
      marginTop: 4
    },
    avatar: {
      margin: 10,
    },
    orangeAvatar: {
      margin: 10,
      color: '#fff',
      backgroundColor: 'orange'
    },
  }
}

/**
 * Aktiviteti 1, ky eshte nje komment per klasen ne vazhdim
 */
export class Compozim extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      a: 0,
      b: 0
    }
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    const shumzimi = 0
    return (
      <div>
        {Children.map(this.props.children, next => {
          return React.cloneElement(next, { agoni: "test"})
        })}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Compozim))
