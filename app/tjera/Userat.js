import React, { Children } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import ListaUserave from 'tjera/ListaUserave'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    row: {
      width: '100%',
      marginTop: 8
    },
    rreshti: {
      marginTop: 4
    },
    avatar: {
      margin: 10,
    },
    orangeAvatar: {
      margin: 10,
      color: '#fff',
      backgroundColor: 'orange'
    },
  }
}

/**
 * Aktiviteti 1, ky eshte nje komment per klasen ne vazhdim
 */
export class Userat extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      items: [],
      search: '',
      error: ''
    }
  }

  onSearchChanged = (event) => {
    this.setState({
      search: event.target.value
    })
  }

  onRequestUsers = (event) => {
    event.preventDefault()
    event.stopPropagation()

    let promise = Promise.resolve([{emri: "tasht", mbiemri: "Lohel"}, { emri: "tjetri", mbiemri: "asfasdghe"}])

    this.setState({
      isLoading: true,
      items: [],
      error: ''
    })
    const { items } = this.state
    setTimeout(() => {

      promise.then((response) => {
        this.setState({
          items: [...items, ...response],
          error: '',
          isLoading: false
        })
      })
    }, 1000)
  }

  onFailUsers = (event) => {
    event.preventDefault()
    event.stopPropagation()

    this.setState({
      isLoading: true,
      error: ''
    })
    let promise = Promise.resolve([{emri: "tasht", mbiemri: "Lohel"}, { emri: "tjetri", mbiemri: "asfasdghe"}])

    const { items } = this.state
    setTimeout(() => {

      promise.then((response) => {
        this.setState({
          items: [],
          error: 'Diqka shkoj gabim',
          isLoading: false
        })
      })
    }, 1000)
  }


  render() {
    let {classes, className, theme: { palette }} = this.props
    const { items, search, error, isLoading } = this.state

    let filtered = items.filter(next => {
      return next.emri.startsWith(search)
    })
    return (
      <div style={{width: '100%'}} className={'col-md-3'}>
        <TextField label={"Search"} placeholder={"Emri..."} fullWidth={true} value={search} onChange={this.onSearchChanged} />
        <ListaUserave users={filtered} />

        {isLoading && <div>Loading....</div>}
        {error && <div>{error}</div>}
        {filtered.length === 0 && <div>
          Nuk ka usera me filterin ton
        </div>}
        <Button onClick={this.onRequestUsers}>
          Loado Userat
        </Button>
        <Button onClick={this.onFailUsers}>
          Fail Userat
        </Button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Userat))
