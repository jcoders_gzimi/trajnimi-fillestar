import React, { Children } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import RreshtiUser from 'tjera/RreshtiUser'

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    row: {
      width: '100%',
      marginTop: 8
    },
    rreshti: {
      marginTop: 4
    },
    avatar: {
      margin: 10,
    },
    orangeAvatar: {
      margin: 10,
      color: '#fff',
      backgroundColor: 'orange'
    },
  }
}

/**
 * Aktiviteti 1, ky eshte nje komment per klasen ne vazhdim
 */
export class UserCard extends React.Component {
  render() {
    let {classes, className, theme: { palette }, user} = this.props
    const shumzimi = 0
    return (
      <RreshtiUser user={user} backgroundColor={'blue'}/>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(UserCard))
