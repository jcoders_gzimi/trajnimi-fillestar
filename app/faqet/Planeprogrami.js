import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import Faqe from 'common/Faqe'
import Userat from 'tjera/Userat'
import Card from "@material-ui/core/Card/Card";
import CardHeader from '@material-ui/core/CardHeader';

const styles = ({palette, typography, spacing, helpers, helpers: { ellipsis }, mixins, shadows, transitions}) => {
  // let ellipsis = helpers.ellipsis
  return {
    titulli: {
      ...typography.title,
      ...ellipsis
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4,
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    }
  }
}

/**
 * Login component
 */
export class Planeprogrami extends React.Component {


  render() {
    let {classes, className, theme: { palette }} = this.props

    return (
        <Faqe>
          <h4 className={classes.titulli}>
            Trajnimi 2 javore per HTML/CSS dhe Javascript
          </h4>
          <div className={classes.subkontent}>
            Trajnimi do te organizohet per nje jave si parapregaditje per zhvillim te projektit per Raiffeisen
          </div>
          <div className={classes.subkontent}>
            Fokusi i ketije trajnimi do te jete te ju jep informacion mbi:
            <ul>
              <li>
                1. HTML
              </li>
              <li>
                2. CSS
              </li>
              <li>
                3. Javascript
              </li>
            </ul>
          </div>
          <div className={classes.subkontent}>
            Pas perfundimit te trajnimit ju do te jeni ne gjendje te:
            <ul>
              <li>
                1. Strukturoni nje web faqe duke perdorur HTML
              </li>
              <li>
                2. Te organizoni User Interface (UI) me ngjyra te bukura, layouts
              </li>
              <li>
                3. Te siguroni logjiken duke perdorur javascript
              </li>
            </ul>
          </div>
          <div className={classes.kontent}>
            Orari dhe organizimi
          </div>

          <div className={classes.subkontent}>
            @GZIM organizo qita
          </div>

          <div className={classes.kontent}>
            Menyra e punes
          </div>

          <div className={classes.subkontent}>
            Te gjithe do te keni qasje ne kod, qe ti kryeni ushtrimet ineraktive ketu ne klase. Prej juve kerkohet qe te ndjekni oret ... (@GZIM vazhdo edhe qet pjese)
          </div>
          <div className={classes.subkontent}>
            Editori i kodit ??
          </div>
          <div className={classes.subkontent}>
            Git LAB etc
          </div>
          <Userat/>
        </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Planeprogrami))
