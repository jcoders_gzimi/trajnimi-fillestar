import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import { Link } from 'react-router-dom'
import classNames from 'classnames'

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    konteineri: {
      width: 340,
      height: '100%',
      overflowY: 'auto',
      background: palette.primary.main,
      display: 'flex',
      flexFlow: 'column nowrap',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      padding: 16
    },
    navigim: {
      width: '100%',
      marginTop: 8,
      marginBottom: 8,
      '&:hover': {
        color: palette.secondary.main,
        '&$info: ': {
          color: palette.secondary.main
        }
      },
      textDecoration: 'none',
      ...typography.subheading
    },
    ndernavigim: {
      width: 'calc(100% - 16px)',
      marginTop: 8,
      marginBottom: 8,
      marginLeft: 16,
      '&:hover': {
        color: palette.secondary.main
      },
      textDecoration: 'none',
      ...typography.subheading
    },
    titulli: {
      color: palette.text.primary,
      ...typography.title
    },
    info: {
      ...typography.subContent,
      margin: 0
    }
  }
}

/**
 * Komponentja e faqes
 */
export class Navigimi extends React.Component {


  render() {
    let {classes, className, theme: { palette }} = this.props
    return (
      <div className={classes.konteineri}>
        <h4 className={classes.titulli}>
          Navigimi
        </h4>
        <Link className={classes.navigim} to={{pathname: "/"}}>
          Planeprogrami dhe informacionet
        </Link>
        <Link className={classes.navigim} to={{pathname: "/html"}}>
          HTML
        </Link>
        <Link className={classes.ndernavigim} to={{pathname: "/html/1"}}>
          HTML Aktiviteti 1
        </Link>
        <Link className={classes.navigim} to={{pathname: "/css"}}>
          CSS
        </Link>
        <Link className={classes.ndernavigim} to={{pathname: "/css/1"}}>
          CSS Aktiviteti 1
        </Link>
        <Link className={classes.navigim} to={{pathname: "/javascript"}}>
          Javascript
        </Link>
        <Link className={classes.ndernavigim} to={{pathname: "/variablat"}}>
          Variablat
        </Link>
        <Link className={classes.ndernavigim} to={{pathname: "/objektet"}}>
          Objektet
        </Link>
        <Link className={classes.ndernavigim} to={{pathname: "/arrays"}}>
          Arrays
        </Link>
        <Link className={classes.ndernavigim} to={{pathname: "/funksionet"}}>
          Funksionet
        </Link>
        {/*<Link className={classes.ndernavigim} to={{pathname: "/aktiviteti1"}}>*/}
          {/*String*/}
        {/*</Link>*/}
        {/*<Link className={classes.ndernavigim} to={{pathname: "/aktiviteti2"}}>*/}
          {/*Aktiviteti 2*/}
        {/*</Link>*/}
        {/*<Link className={classes.ndernavigim} to={{pathname: "/aktiviteti3"}}>*/}
          {/*Aktiviteti 3*/}
        {/*</Link>*/}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Navigimi))
