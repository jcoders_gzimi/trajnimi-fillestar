import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField/TextField";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    }
  }
}

/**
 * Faqja per informatat rreth arrays
 */
export class HTML extends React.Component {

  constructor (props) {
    super(props)
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          Aktiviteti 1
        </h4>
        Une duhet te mbeshtjellem ne h1, sepse jam i vetmuar <br/>
        Une permbaj shuuuuuum tekst, per at arsye kam nevoje te jeme ne nje paragraph
        <h1>
          Une duhet te jeme i kuq (ngjyra e kuqe shume me ngjane)
        </h1>
        <p>
          Une duhet te jeme me madhesi te fontit: 24px (Feeling like HULK)
        </p>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(HTML))
