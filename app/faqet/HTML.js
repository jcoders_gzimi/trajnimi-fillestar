import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField/TextField";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    }
  }
}

/**
 * Faqja per informatat rreth arrays
 */
export class HTML extends React.Component {

  constructor (props) {
    super(props)
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          HTML
        </h4>
        <div className={classes.kontent}>
          HTML eshte shkurtese per Hypertext Markup Language me ane te se ciles ju mund shkruani kod strukturore ne menyre qe te realizoni nje faqe ne internet, sikurse kjo qe jeni duke e shiku. Permes HTML ju mund te specifikoni:
          <ul>
            <li>fontet</li>
            <li>ngjyrat</li>
            <li>grafiken dhe </li>
            <li>linqet ne WWW (world wide web) </li>
          </ul>
        </div>
        <div className={classes.kontent}>
          Konceptet kryesore qe duhet ti dini jane keto:
          <ul>
            <li>HTML describes the structure of Web pages using markup</li>
            <li>HTML elements are the building blocks of HTML pages</li>
            <li>HTML elements are represented by tags</li>
            <li>HTML tags label pieces of content such as "heading", "paragraph", "table", and so on</li>
            <li>Browsers do not display the HTML tags, but use them to render the content of the page</li>
          </ul>
        </div>

        <div className={classes.kontent}>
          Tags
        </div>

        <div className={classes.subkontent}>
          Nepermjet tagjeve ju munde te strukturoni kontentin e nje faqe. Tagjet me te perhapura ne HTML jane:
          <ul>
            <li>Struktura gjenerale e nje tag do te duket keshtu: {'<tagname>kontenti futet ketu...</tagname>'}</li>
            <li>
              Head (Qe ne kod do te duket si: {'<head></head>'}), tag special ku mund te futen modulet, referencat, styles te applikacionit qe mund te importohen nga nje resurs i jashtem ose mbrendhshem
            </li>
            <li>
              Body (Qe ne kod do te duket si: {'<body></body>'}), tag special qe tregon trupin e nje web faqe
            </li>
            <li>
              Div (Qe ne kod do te duket si: {'<div></div>'}), tag per strukturim te kontentit (mendone si ne arkitekture konceptin e dhomes, qdo div nje dhome ne vetvete etj)
            </li>
            <li>
              Img (Qe ne kod do te duket si: {'<img></img>'}), tag per renderim te imazhit
            </li>
            <li>
              h1, h2, h3, h4 (Qe ne kod do te duket si: {'<h1></h1>'}), tag per renderim te titullit<br/>
            </li>
          </ul>
        </div>

        <div className={classes.kontent}>
          Resurset
        </div>

        <div className={classes.subkontent}>
          <a href={'https://www.w3schools.com/html/'}>W3 Schools</a>: Available online me mesime dhe trajnime te ndryshme interaktive per HTML<br/>
          <a href={'https://www.codecademy.com/'}>Code Accademy</a>: Poashtu interaktive me llojellojshmeri te planeprogrameve
        </div>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(HTML))
