import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField/TextField";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    }
  }
}

/**
 * Faqja per informatat rreth arrays
 */
export class CSS extends React.Component {

  constructor (props) {
    super(props)
  }

  render() {
    let {classes, className, theme: { palette }} = this.props

    let syntax = '{ color: blue; font-size: 14px; }'
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          CSS
        </h4>
        <div className={classes.kontent}>
          CSS e zgjidhe problemin e styling, layout, strukturimin dhe organizimin e kontentit<br/>
          Prederisa HTML perdoret me pershkru kontentin, nje header, nje footer, nje image, nje paragraph,<br/>
          CSS perdoret per me definu strukturen. Shembull: renditi paragrafet ne te majt, ngjyren e tekstit bere te <label style={{ color: 'red' }}>kuqe</label>, buttonin bere te gjate per 80px
          <button style={{ widht: 80, marginLeft: 4 }}>Un jam button</button>
        </div>
        <div className={classes.kontent}>
          Sintaksa e CSS duket keshtu: {syntax}
        </div>
        <div className={classes.kontent}>
          Resurset
        </div>
        <div className={classes.subkontent}>
          <a href={'https://www.w3schools.com/css/'}>W3 Schools</a>: Available online me mesime dhe trajnime te ndryshme interaktive per HTML<br/>
          <a href={'https://www.codecademy.com/'}>Code Accademy</a>: Poashtu interaktive me llojellojshmeri te planeprogrameve
        </div>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(CSS))
