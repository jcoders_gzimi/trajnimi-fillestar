import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Chart from "common/Graf";
import Paper from '@material-ui/core/Paper'

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    },
    grafet: {
      display: 'flex',
      flexFlow: 'row wrap',
      alignItems: 'flex-start'
    },
    grafi: {
      margin: 16,
      backgroundColor: 'white',
      width: 360,
      padding: 16
    },
    kanvasi: {
      width: '100%',
      height: 220
    }
  }
}

/**
 * Faqja rreth variablave
 */
export class Funksionet extends React.Component {

  constructor (props) {
    super(props)
  }

  xNeKatrorePlusTre (x) {
    return Math.pow(x, 2) + 3
  }

  dyHereXPlusTre (x) {
    return 2 * x + 3
  }

  vleraAbsoluteEX (x) {
    return Math.abs(x)
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    const x = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    const data1 = x.map(x => {
      return [x, this.dyHereXPlusTre(x)]
    })
    const data2 = x.map(x => {
      return [x, this.xNeKatrorePlusTre(x)]
    })
    const data3 = x.map(x => {
      return [x, this.vleraAbsoluteEX(x)]
    })
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          Funksionet
        </h4>
        <div className={classes.grafet}>
          <Paper className={classes.grafi}>
            <Chart className={classes.kanvasi}
             options={{
              title: {
                text: "2*x + 3"
              },
              xAxis: {
                type: 'value'
              },
              yAxis: {
                type: 'value'
              },
              series: [{
                data: data1,
                type: 'line'
              }]
            }}/>
          </Paper>
          <Paper className={classes.grafi}>
            <Chart className={classes.kanvasi} options={{
              title: {
                text: "x^2 + 3"
              },
              xAxis: {
                type: 'value'
              },
              yAxis: {
                type: 'value'
              },
              series: [{
                data: data2,
                type: 'line'
              }]
            }}/>
          </Paper>
          <Paper className={classes.grafi}>
            <Chart className={classes.kanvasi} options={{
              title: {
                text: "|x|"
              },
              xAxis: {
                type: 'value'
              },
              yAxis: {
                type: 'value'
              },
              series: [{
                data: data3,
                type: 'line'
              }]
            }}/>
          </Paper>
        </div>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Funksionet))
