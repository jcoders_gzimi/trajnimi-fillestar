import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField/TextField";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    }
  }
}

/**
 * Faqja per informatat rreth arrays
 */
export class HTML extends React.Component {

  constructor (props) {
    super(props)
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    let button = '<button>Une jam nje button</button>'
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          Aktiviteti 1
        </h4>
        <div className={classes.kontent}>
          Duke perdorur HTML tags te pershkruhet kontenti i kesaj faqe ne baze te kerkesave:
          <ul>
            <li>
              Rendero nje titull 'h1' me kontentin "Une jam Emri Juaj"
            </li>
            <li>
              Pas titullit te shkruhet kodi qe e shfaqe nje paragraf me kontentin "Une jam nje paragrapf, permbaje tekst te qfaredoshem"
            </li>
            <li>
              Pas paragrafit te shkruhet nje kodi qe e renderon nje imazh, reference qe e ka kete imazh si href:<br/>
              https://news.nationalgeographic.com/content/dam/news/2018/05/17/you-can-train-your-cat/02-cat-training-NationalGeographic_1484324.ngsversion.1526587209178.adapt.1900.1.jpg
            </li>
            <li>
              Provo re renderosh nje button duke perdorur: {button}, ndrysho kontentin ne I am clickable
            </li>
            <li>
              @Gzim shto sene qitu ala
            </li>
          </ul>
        </div>

        <div className={classes.kontent}>
          Kontenti i juaj te vazhdoje ketu:
        </div>

        <h1>Titulli?</h1>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(HTML))
