import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField/TextField";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    }
  }
}

/**
 * Faqja rreth informatave te objekteve
 */
export class Variablat extends React.Component {

  constructor (props) {
    super(props)
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          Javascript
        </h4>
        <div className={classes.kontent}>
          JavaScript is one of the 3 languages all web developers must learn:

          <uL>
            <li>
              1. HTML to define the content of web pages
            </li>
            <li>
              2. CSS to specify the layout of web pages
            </li>
            <li>
              3. JavaScript to program the behavior of web pages
            </li>
          </uL>
        </div>
        <div className={classes.kontent}>
          Resurset
        </div>

        <div className={classes.subkontent}>
          <a href={'https://www.w3schools.com/js/'}>W3 Schools</a>: Available online me mesime dhe trajnime te ndryshme interaktive per HTML<br/>
          <a href={'https://www.codecademy.com/'}>Code Accademy</a>: Poashtu interaktive me llojellojshmeri te planeprogrameve
        </div>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Variablat))
