import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField";
import Avatar from "@material-ui/core/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    seksion: {
      ...typography.subheading,
      marginTop: 24
    },
    kontent: {
      ...typography.subheading
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16
    }
  }
}

/**
 * Faqja rreth variablave
 */
export class Variablat extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      a: 0,
      b: 0,
      teksti: ''
    }
  }

  onNumberChanged = (field, event) => {
    const value = event.target.value
    if (!isNaN(value)) {
      this.setState({
        [field]: parseInt(value)
      })
    }
  }

  onTextChanged = (field, event) => {
    const value = event.target.value
    this.setState({
      [field]: value
    })
  }

  render() {
    let {classes, className, theme: { palette }} = this.props
    let { a, b, teksti } = this.state

    let shuma = a + b
    let shumezimi = a * b
    let pjestimi = 0
    if (b !== 0) {
      pjestimi = a / b
    }

    let shkronjaPare = ''
    if (teksti.length > 0) {
      shkronjaPare = teksti.charAt(0)
    }
    let shkronjaFundit = ''
    if (teksti.length > 0) {
      shkronjaFundit = teksti.charAt(teksti.length - 1)
    }
    let gjatesia = teksti.length
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          Variablat ne javascript
        </h4>
        <div className={classes.kontent}>
          Ketu do te mesoni si te shumezoni dy variabla per me fitu rezultatin
        </div>
        <TextField
          label={"Variabla A"}
          value={a}
          onChange={this.onNumberChanged.bind(this, 'a')}
          className={classes.row}>
        </TextField>
        <TextField
          label={"Variabla B"}
          value={b}
          onChange={this.onNumberChanged.bind(this, 'b')}
          className={classes.row}>
        </TextField>
        <div className={classes.kontent}>
          Rezultati a + b:
        </div>
        <div className={classes.subkontent}>
          {shuma}
        </div>
        <div className={classes.kontent}>
          Rezultati a * b:
        </div>
        <div className={classes.subkontent}>
          {shumezimi}
        </div>
        <div className={classes.kontent}>
          Rezultati a / b:
        </div>
        <div className={classes.subkontent}>
          {pjestimi}
        </div>
        <div className={classes.seksion}>
          Ketu do te mesoni si te manipuloni tekst
        </div>
        <TextField
          label={"Teksti"}
          value={teksti}
          onChange={this.onTextChanged.bind(this, 'teksti')}
          className={classes.row}>
        </TextField>
        <div className={classes.kontent}>
          Rezultati gjatesia:
        </div>
        <div className={classes.subkontent}>
          {gjatesia}
        </div>
        <div className={classes.kontent}>
          Rezultati shkronja e pare:
        </div>
        <div className={classes.subkontent}>
          {shkronjaPare}
        </div>
        <div className={classes.kontent}>
          Rezultati shkronja e fundit:
        </div>
        <div className={classes.subkontent}>
          {shkronjaFundit}
        </div>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Variablat))
