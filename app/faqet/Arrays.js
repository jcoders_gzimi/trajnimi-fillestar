import React from "react";
import {connect} from "react-redux";
import {withStyles} from "@material-ui/core/styles";
import classNames from 'classnames'
import Faqe from 'common/Faqe'

import Paper from '@material-ui/core/Paper'
import TextField from "@material-ui/core/TextField/TextField";
import Avatar from "@material-ui/core/Avatar/Avatar";

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    titulli: {
      ...typography.title
    },
    kontent: {
      ...typography.subheading,
      marginTop: 4
    },
    subkontent: {
      ...typography.subheading,
      marginLeft: 16,
      marginTop: 4
    }
  }
}

/**
 * Faqja per informatat rreth arrays
 */
export class Arrays extends React.Component {

  constructor (props) {
    super(props)
  }

  render() {
    let {classes, className, theme: { palette }} = this.props

    const userat = [
      {
        emri: "Agon",
        mbiemri: "Lohaj",
        gjinia: 'm',
        pare: 50
      },
      {
        emri: "Fiona",
        mbiemri: "Shehu",
        gjinia: 'f',
        pare: 100
      },
      {
        emri: "Gzim",
        mbiemri: "Mehmeti",
        gjinia: 'm',
        pare: 90
      }
    ]
    const meshkujt = userat.filter(next => next.gjinia === 'm')
    const femrat = userat.filter(next => next.gjinia === 'f')
    return (
      <Faqe>
        <h4 className={classes.titulli}>
          Arrays
        </h4>

        <div className={classes.kontent}>
          Userat ne databaze
        </div>
        <div className={classes.subkontent}>
          {userat.map(next => next.emri).join(', ')}
        </div>
        <div className={classes.kontent}>
          Userat te gjinise mashkuller
        </div>
        <div className={classes.subkontent}>
          {meshkujt.map(next => next.emri).join(', ')}
        </div>
        <div className={classes.kontent}>
          Userat te gjinise femrore
        </div>
        <div className={classes.subkontent}>
          {femrat.map(next => next.emri).join(', ')}
        </div>
        <div className={classes.kontent}>
          Emri + Mbieri
        </div>
        <div className={classes.subkontent}>
          {userat.map(next => next.emri + ' ' + next.mbiemri).join(', ')}
        </div>
      </Faqe>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Arrays))
