import React from "react";
import { connect } from "react-redux";
import withStyles from '@material-ui/core/styles/withStyles'
import compose from 'recompose/compose'

const styles = ({typography}) => ({
  app: {
    fontFamily: typography.fontFamily,
    height: '100vh',
    width: '100vw',
    maxHeight: '100vh',
    maxWidth: '100vw',
    overflow: 'hidden',
    display: 'flex',
    flexFlow: 'row nowrap',
    alignContent: 'flex-start',
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  }
})
/**
 * The app container
 */
class App extends React.Component {
  /**
   * propTypes
   * @type {object}
   */
  static get propTypes() {
    return {
    }
  }

  /**
   * render
   * @returns {ReactElement} markup
   */
  render() {
    const { classes } = this.props
    return (
      <div className={classes.app}>
        {this.props.children}
      </div>
    )
  }
}

let mapStateToProps = function (state) {
  return {
  }
}

let mapDispatchToProps = {
}
export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(App)
