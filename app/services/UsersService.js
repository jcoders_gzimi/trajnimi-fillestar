import {CALL_API} from 'common/middleware/api'

/**
 * The service providing user related services
 * @class UserService
 */
class UserService {

  /**
   * constructor
   * @method constructor
   * @param dispatch - the dispatch function for redux store
   */
  constructor(dispatch) {
    this.dispatch = dispatch
  }

  all (which) {
    return this.dispatch({
      [CALL_API]: {
        endpoint: '/v1/users/all',
        options: {}
      }
    })
  }

  save (which) {
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(which)
    }

    return this.dispatch({
      [CALL_API]: {
        endpoint: '/v1/users',
        options: options
      }
    })
  }

  delete (which) {
    const options = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(which)
    }

    return this.dispatch({
      [CALL_API]: {
        endpoint: '/v1/users',
        options: options
      }
    })
  }
}

export default UserService
