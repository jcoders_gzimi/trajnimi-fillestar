import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { Route, Switch } from "react-router";
import store, { history } from "common/Store";
import injectTapEventPlugin from "react-tap-event-plugin";
import CssBaseline from "@material-ui/core/CssBaseline";
import ThemeProvider from "ThemeProvider";
import App from "App";

// import Aktiviteti1 from "faqet/Aktiviteti1";
// import Aktiviteti2 from "faqet/Aktiviteti2";
// import Aktiviteti3 from "faqet/Aktiviteti3";
import Planeprogrami from "faqet/Planeprogrami";
import Variablat from "faqet/Variablat";
import Objektet from "faqet/Objektet";
import Arrays from "faqet/Arrays";
import HTML from "faqet/HTML";
import HTML1 from "faqet/HTML1";
import CSS from "faqet/CSS";
import CSS1 from "faqet/CSS1";
import Javascript from "faqet/Javascript";
import Funksionet from "faqet/Funksionet";

import "assets/fonts/roboto-black.css";
import "assets/fonts/roboto-bold.css";
import "assets/fonts/roboto-regular.css";
import "assets/css/app.css";
import "bootstrap/dist/css/bootstrap.min.css";

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

const AppContainer = () => {
  return (
    <Provider store={store}>
      <ThemeProvider>
        <App>
          <CssBaseline/>
          <ConnectedRouter history={history}>
            <Switch>
              {/*<Route exact path="/aktiviteti1" component={Aktiviteti1}/>*/}
              {/*<Route exact path="/aktiviteti2" component={Aktiviteti2}/>*/}
              {/*<Route exact path="/aktiviteti3" component={Aktiviteti3}/>*/}
              <Route exact path="/html" component={HTML}/>
              <Route exact path="/html/1" component={HTML1}/>
              <Route exact path="/css" component={CSS}/>
              <Route exact path="/css/1" component={CSS1}/>
              <Route exact path="/javascript" component={Javascript}/>
              <Route exact path="/variablat" component={Variablat}/>
              <Route exact path="/objektet" component={Objektet}/>
              <Route exact path="/arrays" component={Arrays}/>
              <Route exact path="/funksionet" component={Funksionet}/>
              <Route path="/" component={Planeprogrami}/>
            </Switch>
          </ConnectedRouter>
        </App>
      </ThemeProvider>
    </Provider>
  )
}
ReactDOM.render(<AppContainer/>, document.getElementById('container'))
