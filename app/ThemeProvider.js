import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {MuiThemeProvider} from '@material-ui/core/styles'
import {createMuiTheme} from '@material-ui/core/styles';
import {fade} from '@material-ui/core/styles/colorManipulator'

/**
 * A component which provides the theme of the app for the Material UI components
 */
export class ThemeProvider extends React.Component {
  /**
   * propTypes
   * @type {object}
   * @property {object} theme - redux store theme
   * @property {function} updateTheme - updateTheme with the new width
   */
  static get propTypes() {
    return {
      // provided by redux store
    }
  }

  /**
   * constructor
   * constructor
   * @param {object} props
   */
  constructor(props) {
    super(props)
  }

  /**
   * Assign the document body background color to the new theme background color
   */
  componentWillMount() {
  }

  /**
   * Assign the document body background color to the new theme background color
   */
  componentDidUpdate() {
  }

  /**
   * Wraps the children with a MuiThemeProvider for material ui theme provider
   * @returns {ReactElement} markup
   */
  render() {
    let muiTheme = createMuiTheme({
      palette: {
        primary: {
          main: '#38AECC',
          light: '#B3E5FC',
          contrastText: '#fff'
        },
        secondary: {
          main: '#FF7070',
          contrastText: fade('#fff', 0.7)
        },
        text: {
          inverted: fade('#fff', 0.7),
          invertedDisabled: fade('#fff', 0.4),
        },
        navBg: '#182e3d',
        drawerBG: '#0f1f29',
        borderColor: '#e4e4e4'
      },
      helpers: {
        ellipsis: {
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        },
        flex: {
          rowWrapStart: {
            display: 'flex',
            flexFlow: 'row wrap',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            alignContent: 'flex-start'
          },
          rowNoWrapStart: {
            display: 'flex',
            flexFlow: 'row nowrap',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            alignContent: 'flex-start'
          },
          rowNoWrapEnd: {
            display: 'flex',
            flexFlow: 'row nowrap',
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
            alignContent: 'flex-end'
          },
          rowNoWrapCenterBetween: {
            display: 'flex',
            flexFlow: 'row nowrap',
            alignItems: 'center',
            justifyContent: 'space-between',
            alignContent: 'flex-start'
          },
          rowNoWrapStretch: {
            display: 'flex',
            flexFlow: 'row nowrap',
            alignItems: 'stretch',
            justifyContent: 'stretch',
            alignContent: 'flex-start'
          },
          columnWrapStart: {
            display: 'flex',
            flexFlow: 'column wrap',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            alignContent: 'flex-start'
          },
          columnNoWrapStart: {
            display: 'flex',
            flexFlow: 'column nowrap',
            alignItems: 'flex-start',
            justifyContent: 'flex-start',
            alignContent: 'flex-start'
          },
          columnNoWrapCenter: {
            display: 'flex',
            flexFlow: 'column nowrap',
            alignItems: 'center',
            justifyContent: 'center',
            alignContent: 'flex-start'
          },
          rowWrapCenter: {
            display: 'flex',
            flexFlow: 'row wrap',
            alignContent: 'flex-start',
            alignItems: 'center',
            justifyContent: 'center'
          },
          rowNoWrapCenterV: {
            display: 'flex',
            flexFlow: 'row nowrap',
            alignContent: 'flex-start',
            alignItems: 'center',
            justifyContent: 'flex-start'
          },
          rowNoWrapCenterH: {
            display: 'flex',
            flexFlow: 'row nowrap',
            alignContent: 'flex-start',
            alignItems: 'flex-start',
            justifyContent: 'center'
          },
          rowNoWrapCenter: {
            display: 'flex',
            flexFlow: 'row nowrap',
            alignContent: 'flex-start',
            alignItems: 'center',
            justifyContent: 'center'
          }
        }
      },
      spacing: {
        baseRadius: 10
      },
    })

    muiTheme = {
      ...muiTheme,
      typography: {
        ...muiTheme.typography,
        subContent: muiTheme.typography.body1
      },
      spacing: {
        ...muiTheme.spacing,
      }
    }
    console.log('THEME', muiTheme)
    return (
      <MuiThemeProvider theme={ muiTheme }>
        { this.props.children }
      </MuiThemeProvider>
    )
  }
}

var mapStateToProps = function (state) {
  return {
  }
}

var mapDispatchToProps = function (dispatch) {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThemeProvider)
