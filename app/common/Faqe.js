import React from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";
import classNames from 'classnames'
import Navigimi from 'faqet/Navigimi'

const styles = ({palette, typography, spacing, helpers: {flex}, mixins, shadows, transitions}) => {
  return {
    root: {
      width: '100%',
      minHeight: '100vh',
      display: 'flex',
      height: '100%',
      flexFlow: 'row nowrap'
    }
  }
}

/**
 * Komponentja e faqes
 */
export class Faqe extends React.Component {

  constructor (props) {
    super(props);

  }

  render() {
    let {classes, className, theme: { palette }, children} = this.props
    return (
      <div className={classNames(classes.root, className)}>
        <Navigimi/>
        <div style={{
          flex: 1,
          display: 'flex',
          flexFlow: 'column nowrap',
          justifyContent: 'flex-start',
          alignContent: 'flex-start',
          margin: 16,
          overflow: 'auto'
        }}>
          {children}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = {}
export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(Faqe))
