/**
 * Created by Agon on 2/2/2017.
 */
import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import echarts from 'echarts'
import { connect } from 'react-redux'
import withStyles from '@material-ui/core/styles/withStyles'
import compose from 'recompose/compose'
import classNames from 'classnames'
const styles = {
  chartWrapper: {
    width: '100%',
    height: '100%',
    display: 'flex'
  }
}
/**
 * Chart Component
 */
class Chart extends React.Component {

  /**
   * propTypes
   * @type {object}
   * @property {object} options - the graph options
   * @property {string} actionId - the state actionId
   * @property {function} onInit - chart is ready to receive props
   */
  static get propTypes () {
    return {
      // activeFilters: PropTypes.arrayOf(PropTypes.object).isRequired,
      options: PropTypes.object,
      actionId: PropTypes.string,
      onInit: PropTypes.func,
      // provided by redux store
    }
  }

  constructor (props) {
    super(props)
    /**
     * the chart used to display the data
     */
    this.chart = null
  }

  /**
   * init the chart ({@link LineChart#init}) and resize event listener
   */
  componentDidMount () {
    this.init()
    this.setOption()
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.actionId !== this.props.actionId) {
      this.setOption()
    }
    this.onChartResize()
  }

  onChartResize () {
    let width = ReactDOM.findDOMNode(this.refs.container).clientWidth
    let height = ReactDOM.findDOMNode(this.refs.container).clientHeight
    if (this.chart) {
      this.chart.resize()
    }
    let {onResize} = this.props
    if (onResize) {
      onResize(width, height)
    }
  }

  /**
   * on component unmount dispose the chart ({@link LineChart#dispose})
   */
  componentWillUnmount () {
    this.dispose()
  }

  /**
   * init the chart and set chart options ({@link LineChart#setOption})
   */
  init () {
    this.chart = echarts.init(this.refs.container)
    if (this.props.onInit) {
      this.props.onInit(this.chart)
    }
  }

  setOption () {
    let options = this.props.options
    if (!options) {
      if (this.chart) {
        this.chart.clear()
      }
      return
    }
    console.log('setting options')
    console.log(options)
    this.chart.showLoading()
    this.chart.clear()
    try {
      this.chart.setOption(options, true, false)
    } catch (e) {
      console.log('error setting options')
      console.log(e)
    }
    this.chart.hideLoading()
    if (this.props.onOptionsChanged) {
      this.props.onOptionsChanged()
    }
  }

  /**
   * clear the chart resources
   */
  dispose () {
    if (this.chart) {
      this.chart.dispose()
      this.chart = null
    }
  }

  /**
   * renders a container for the graph and a legend
   * @returns {ReactElement} markup
   */
  render () {
    let {style, classes, children, className: classNameProp, onResize} = this.props
    return (
      <div className={classNames(classes.chartWrapper, classNameProp)} style={style}>
        <div className={classes.chartWrapper} ref={'container'}/>
        {children}
      </div>
    )
  }
}

let mapStateToProps = function (state) {
  return {}
}

let mapDispatchToProps = {

}
export default compose(
  withStyles(styles, {withTheme: true}),
  connect(mapStateToProps, mapDispatchToProps)
)(Chart)