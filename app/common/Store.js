import {applyMiddleware, combineReducers, createStore} from "redux";
import thunkMiddleware from "redux-thunk";
import createLogger from "redux-logger";
import {routerMiddleware, routerReducer} from "react-router-redux";
import createBrowserHistory from 'history/createBrowserHistory'
import createHashHistory from 'history/createHashHistory'
import {createSession} from "redux-session";
import api from "common/middleware/Api";

import user from "reducers/User";
const history =  process.env.NODE_ENV === 'development' ? createHashHistory() : createBrowserHistory()
// const history =  createBrowserHistory()


const loggerMiddleware = createLogger()

const reducers = combineReducers({
  routing: routerReducer,
  user,
})
const session = createSession({
  ns: 'TEMPLATE_STORAGE',
  throttle: 0,
  selectState (state) {
    return {
      user: state.user
    }
  },
  clearStorage (action) {
    return action.type === 'SOME_ACTION_TO_CLEAR_STORAGE'
  }
})

const middleware = [
  thunkMiddleware,
  routerMiddleware(history),
  // process.env.NODE_ENV === 'development' && loggerMiddleware,
  loggerMiddleware,
  api,
  session
].filter(Boolean)

const store = createStore(reducers, applyMiddleware(...middleware))
/**
 * The browsing history
 */
export {
  history
}
/**
 * The redux store which combines all the reducers
 */
export default store
