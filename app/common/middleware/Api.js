import { API_URL } from 'Constants'
import 'whatwg-fetch'
import _ from 'underscore'

function callApi(endpoint, options = {}, store) {
  // in case we need authorization with tokens, uncomment this
  // var user = store.getState().user
  // if(user && user.session) {
  //   var session = user.session
  //   options.headers = Object.assign({}, options.headers, {
  //     'Authorization': session.token_type + ' ' + session.access_token
  //   })
  // }
  // options.headers = Object.assign({}, options.headers, {
  //   'Accept-Language': store.getState().locale.lang
  // })

  console.log('fetching:')
  console.log(API_URL + endpoint)
  console.log(options)
  return fetch(API_URL + endpoint, options)
    .then(response => {
      if (response.status >= 200 && response.status < 300) {
        return response
      } else {
        if(response.status == 401) {
          // refersh the token of the user
          store.dispatch(logoutAndClearSession(true, true))
        }
        return response.json().then((body) => {
          let message = body && body.message
          if(_.isArray(message)) {
            message = message.join(", ")
          }
          console.log({
            code: response.status,
            message: message || response.statusText
          })
          throw {
            code: response.status,
            message: message || response.statusText
          }
        }, (error) => {
          console.log({
            code: response.status,
            message: response.statusText
          })
          throw {
            code: response.status,
            message: response.statusText
          }
        })
      }
    })
    .then(response => response.json())
    .then(json => {
      console.log('Success at: ', API_URL, endpoint)
      console.log(json)
      return json
    })
}

export const CALL_API = Symbol('Call API')

export default store => next => action => {
  const callAPI = action[CALL_API]

  // So the middleware doesn't get applied to every single action
  if (typeof callAPI === 'undefined') {
    return next(action)
  }

  let { endpoint, options } = callAPI

  return callApi(endpoint, options, store)
}