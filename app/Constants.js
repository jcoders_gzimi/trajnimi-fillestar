/*
 This file contains all constants in the app.
 */
let window = global.window || {}

module.exports = {
  BATCH_SIZE: window.batch_size || 10000,
  API_URL: window.host_name || 'http://localhost:9000',
}
