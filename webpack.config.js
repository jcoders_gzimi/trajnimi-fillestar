const path = require('path')

const BUILD_DIR = path.resolve(__dirname, 'build')
const APP_DIR = path.resolve(__dirname, 'app')

const BUILD_ENV = process.env.NODE_ENV || 'development'
const config = {
  mode: BUILD_ENV,
  entry: APP_DIR + '/index.js',
  output: {
    path: BUILD_DIR,
    publicPath: '',
    filename: 'bundle.js'
  },
  externals: {
    'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  devServer: {
    port: 8080
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.jsx?/,
        include: APP_DIR,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env', 'react','stage-2']
          }
        }
      }
    ]
  },
  resolve: {
    extensions: ['.css', 'json', '.js', '.jsx'],
    modules: [
      'node_modules',
      path.resolve(APP_DIR)
    ]
  }
}

config.module.rules.push(
{
  test: /\.woff(\?.*)?$/,
  use: {
    loader: 'url-loader',
    options: {
      prefix: 'fonts/',
      name: 'fonts/[name].[ext]',
      mimetype: 'application/font-woff',
      limit: 10000
    }
  }
},
{
  test: /\.woff2(\?.*)?$/,
  use: {
    loader: 'url-loader',
    options: {
      prefix: 'fonts/',
      name: 'fonts/[name].[ext]',
      mimetype: 'application/font-woff2',
      limit: 10000
    }
  }
},
{
  test: /\.otf(\?.*)?$/,
  use: {
    loader: 'file-loader',
    options: {
      prefix: 'fonts/',
      name: 'fonts/[name].[ext]',
      mimetype: 'font/opentype'
    }
  }
},
{
  test: /\.ttf(\?.*)?$/,
  use: {
    loader: 'url-loader',
    options: {
      prefix: 'fonts/',
      name: 'fonts/[name].[ext]',
      mimetype: 'application/octet-stream',
    }
  }
},
{
  test: /\.eot(\?.*)?$/,
  use: {
    loader: 'file-loader',
    options: {
      prefix: 'fonts/',
      name: 'fonts/[name].[ext]',
    }
  }
},
{
  test: /\.svg(\?.*)?$/,
  use: {
    loader: 'url-loader',
    options: {
      name: 'images/[name].[ext]',
      mimetype: 'image/svg+xml',
      prefix: 'images/',
      limit: 10000
    }
  }
},
{
  test: /\.(png|jpg|gif)$/,
  use: {
    loader: 'url-loader',
    options: {
      name: 'images/[name].[ext]',
      limit: 10000
    }
  }
}
)

module.exports = config
